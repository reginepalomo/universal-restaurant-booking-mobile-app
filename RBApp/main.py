#Kivy imports
from kivy.core.window import Window
from kivy.lang import Builder

from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.widget import Widget

from kivy.storage.jsonstore import JsonStore

from kivy.utils import get_color_from_hex

#KivyMD imports
from kivymd.app import MDApp
from kivymd.uix.dialog import MDDialog
from kivymd.uix.chip import MDChip
from kivymd.uix.chip import MDChooseChip
from kivymd.uix.picker import MDDatePicker
from kivymd.uix.button import MDFlatButton

from kivymd.uix.list import MDList, ThreeLineListItem

#python imports
import re
from datetime import datetime

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#From libs folder

# General Screens
from libs.screens.LoginScreen import LoginScreen
from libs.screens.SignupScreen import SignupScreen
from libs.screens.SignupCustomerScreen import SignupCustomerScreen
from libs.screens.SignupManagerScreen import SignupManagerScreen
from libs.screens.SignupManagerScreen2 import SignupManagerScreen2


# Customer Screens
from libs.screens.customer_screens.ReservationsScreen import ReservationsScreen
from libs.screens.customer_screens.ReservationItemScreen import ReservationItemScreen
from libs.screens.customer_screens.RestaurantsScreen import RestaurantsScreen
from libs.screens.customer_screens.RestaurantItemScreen import RestaurantItemScreen
from libs.screens.customer_screens.RestaurantsSearchScreen import RestaurantsSearchScreen
from libs.screens.customer_screens.RestaurantSearchItemScreen import RestaurantSearchItemScreen
from libs.screens.customer_screens.CustomerProfileScreen import CustomerProfileScreen
from libs.screens.customer_screens.CreateReservationScreen import CreateReservationScreen
from libs.screens.customer_screens.CreateReservationScreen2 import CreateReservationScreen2

# Manager Screens
from libs.screens.manager_screens.ManagerScheduleScreen import ManagerScheduleScreen
from libs.screens.manager_screens.ManagersScreen import ManagersScreen
from libs.screens.manager_screens.ManagerReservationScreen import ManagerReservationScreen
from libs.screens.manager_screens.UserItemScreen import UserItemScreen
from libs.screens.manager_screens.ManagerProfileScreen import ManagerProfileScreen


#Setting Window size
Window.size = (288, 565)

# ------------------------------------------------------------------
# Adding screens to the ScreenManager
sm = ScreenManager()
sm.add_widget(LoginScreen(name="LoginScreen"))
sm.add_widget(SignupScreen(name="SignupScreen"))
sm.add_widget(SignupCustomerScreen(name="SignupCustomerScreen"))
sm.add_widget(SignupManagerScreen(name="SignupManagerScreen"))
sm.add_widget(SignupManagerScreen2(name="SignupManagerScreen2"))

#Customer Screens
sm.add_widget(ReservationsScreen(name="ReservationsScreen"))
sm.add_widget(ReservationItemScreen(name="ReservationItemScreen"))
sm.add_widget(RestaurantsScreen(name="RestaurantsScreen"))
sm.add_widget(RestaurantItemScreen(name="RestaurantItemScreen"))
sm.add_widget(RestaurantsSearchScreen(name="RestaurantsSearchScreen"))
sm.add_widget(RestaurantSearchItemScreen(name="RestaurantSearchItemScreen"))
sm.add_widget(CustomerProfileScreen(name="CustomerProfileScreen"))
sm.add_widget(CreateReservationScreen(name="CreateReservationScreen"))
sm.add_widget(CreateReservationScreen2(name="CreateReservationScreen2"))

#Manager Screens
sm.add_widget(ManagerScheduleScreen(name="ManagerScheduleScreen"))
sm.add_widget(ManagersScreen(name="ManagersScreen"))
sm.add_widget(ManagerReservationScreen(name="ManagerReservationScreen"))
sm.add_widget(UserItemScreen(name="UserItemScreen"))
sm.add_widget(ManagerProfileScreen(name="ManagerProfileScreen"))


#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations


# -----------------------------------------------------------------
# fields sample
manager_sample = {
	"_id" : "1",
	"username" : "1",
	"password" : "1",
	"email" : "1",
	"restaurant_name" : "1",
	"restaurant_location" : "1",
	"restaurant_cuisine" : "1",
	"restaurant_hours" : "1",
	"restaurant_days" : "1",
	"restaurant_contactnum" : "1",
	"restaurant_desc" : "1"
}

customer_sample = {
	"_id" : "1",
	"username" : "1",
	"password" : "1",
	"email" : "1",
	"customer_name" : "1",
	"customer_contactnum" : "1"	
}

reservation_sample = {
	"_id" : "1",
	"restaurant_name" : "1",
	"customer_user" : "1",
	"reservation_hours" : "1",
	"reservation_date" : "1",
	"reservation_note" : "1",
	"reservation_seats" : "1"
}

# ------------------------------------------------------------------

store = JsonStore('temp.json')
store['user'] = {
				'user_id': "",
				'username': "",
				'role' : ""
			}


class MainApp(MDApp):
	dialog=None
	date = "2021-06-30"
	capacity = ""
	
	def build(self):
		self.screen = Builder.load_file("libs/kv/app_kv.kv")
		return self.screen

	# create customer
	def createCustomer(self):
		res = SignupCustomerScreen.create_customer(self)
		return res
		
	# create manager
	def createManager(self):
		res = SignupManagerScreen2.create_manager(self)
		return res

	def checkLogin(self):
		res = LoginScreen.checkUser(self)
		return res

	def get_user(self, email):
		'''
			Gets the user id from the database during login.
		'''
		curr_user = customers.find_one({"email": email})
		if curr_user is None:
			curr_user = managers.find_one({"email": email})
			print(curr_user["username"])
			
			store['user'] = {
				'username': curr_user["username"],
				'role' : "manager"
			}
			
		else:			
			store['user'] = {
				'username': curr_user["username"],
				'role' : "customer"
			}


	def get_home_screen(self):
		'''
			Sets the home screen for the application
			depending on what type of user logged in.
		'''
		role = store['user']['role']
		if role == "customer":
			return "ReservationsScreen" 
		else:
			return "ManagerScheduleScreen"

	def clear_screen(self):
		#clear the text fields in login screen

		self.screen.get_screen('LoginScreen').ids.email.text = ""
		self.screen.get_screen('LoginScreen').ids.password.text = ""

		self.change_screen("LoginScreen","right")

	def change_screen(self, new_screen, direction):
		'''
			Function that allows the buttons to move to the different screens.
			Since there is only one ScreenManager we can just get LoginScreen's manager.
		'''
		self.screen.get_screen("LoginScreen").manager.current = new_screen
		self.screen.get_screen("LoginScreen").manager.transition.direction = direction

	def change_screen2(self, new_screen):
		'''
			Function that allows the buttons to move to the different screens without the direction parameter.
			Since there is only one ScreenManager we can just get LoginScreen's manager.
		'''
		self.screen.get_screen("LoginScreen").manager.current = new_screen
		self.dialog.dismiss()
	
	def dialog_close(self, item_id):
		self.dialog.dismiss()


	# ------------------------- #
	# CUSTOMER SCREEN FUNCTIONS #
	# ------------------------- #
	def set_reservation_item(self, item_id):
		'''
			Getting the reservation item id fo be passed when 
			checking a reservation's details.
		'''
		ReservationItemScreen.set_item(self, item_id)

	def cancel_reservation(self):
		ReservationItemScreen.cancel_reservation(self)

	def set_restaurant_item(self, item_id):
		'''
			Getting the restaurant item id fo be passed when 
			checking a restaurant's details (from all restaurants).
		'''
		RestaurantItemScreen.set_item(self, item_id)

	def set_restaurant_search_item(self, item_id):
		'''
			Getting the restaurant item id fo be passed when 
			checking a restaurant's details (from search).
		'''
		RestaurantSearchItemScreen.set_item(self, item_id)		

	def search(self):
		#Getting the results after search
		result = RestaurantsSearchScreen.searchResto(self)
		return result

	def get_restoItem(self):

		#Getting the name of the restaurant to be viewed
		resto = self.screen.get_screen("RestaurantItemScreen").ids.restaurant_name.text

		CreateReservationScreen.get_resto_name(self,resto)
		self.change_screen("CreateReservationScreen","left")

	def on_save(self, instance, value, date_range):
		'''
		Events called when the "OK" dialog box button is clicked.

		:type instance: <kivymd.uix.picker.MDDatePicker object>;

		:param value: selected date;
		:type value: <class 'datetime.date'>;

		:param date_range: list of 'datetime.date' objects in the selected range;
		:type date_range: <class 'list'>;
		'''
		global date
		date = value
		CreateReservationScreen2.get_date(self,str(value))


	def on_cancel(self, instance, value):
		'''Events called when the "CANCEL" dialog box button is clicked.'''

	def show_date_picker(self):
		min_date = datetime.strptime("2021:06:30",'%Y:%m:%d').date()
		max_date = datetime.strptime("2060:06:30",'%Y:%m:%d').date()
		date_dialog = MDDatePicker(min_date=min_date,max_date=max_date,primary_color=get_color_from_hex("#DAA520"),
			selector_color=get_color_from_hex("#DAA520"))
		date_dialog.bind(on_save=self.on_save, on_cancel=self.on_cancel)
		date_dialog.open()

	def show_time(self):
		
		self.screen.get_screen("CreateReservationScreen").ids.reserve_card.clear_widgets()

		resto_name = self.screen.get_screen("RestaurantItemScreen").ids.restaurant_name.text
		restaurant = managers.find_one({"restaurant_name": resto_name})
		resto_hrs = restaurant['restaurant_hours']
		manager = restaurant['username']

		time = re.findall(r'\d+', resto_hrs)

		start_time = int(time[0])
		end_time = int(time[1])

		res_filter = {"$and" : [
			{"restaurant_user" : manager},
			{"reservation_date" : str(date)}
			]}
		curr_reservations = reservations.find(res_filter)

		res_capacity = int(restaurant["restaurant_cap"])

		for r in curr_reservations:
			res_capacity = res_capacity - r["reservation_seats"]
			print("capleft: " + str(res_capacity))

		for counter in range(start_time,end_time + 13):
			if counter < 12:
				str_counter = str(counter) + "AM"
				if res_capacity > 0 :
					print("hascap: " + str(res_capacity))
					self.screen.get_screen("CreateReservationScreen").ids.reserve_card.add_widget(		
						MDChip( 
							text= str_counter,icon='clock', selected_chip_color=get_color_from_hex("#1C9554"),
							check=True,color=(112/255.0,211/255.0,47/255.0,1), on_release=self.fetch,
							width="20dp")
						)

				if res_capacity <= 0 :
					print("nocap: " + str(res_capacity))
					str_counter = str(counter) + " AM"
					self.screen.get_screen("CreateReservationScreen").ids.reserve_card.add_widget(		
						MDChip(
							text= str_counter,icon='clock',color=(253/255.0,78/255.0,72/255.0,1), 
							on_release=self.unavailable)
						)
			if counter > 12:
				counter = counter - 12
				str_counter = str(counter) + "PM"
				if res_capacity > 0 :
					print("hascap: " + str(res_capacity))
					self.screen.get_screen("CreateReservationScreen").ids.reserve_card.add_widget(		
						MDChip( 
							text= str_counter,icon='clock', selected_chip_color=get_color_from_hex("#1C9554"),
							check=True,color=(112/255.0,211/255.0,47/255.0,1),on_release=self.fetch)
						)

				if res_capacity <= 0 :
					print("nocap: " + str(res_capacity))
					str_counter = str(counter) + " AM"
					self.screen.get_screen("CreateReservationScreen").ids.reserve_card.add_widget(		
						MDChip(
							text= str_counter,icon='clock',color=(253/255.0,78/255.0,72/255.0,1), 
							on_release=self.unavailable)
						)

			if counter == 12:
				str_counter = str(counter) + "PM"
				if res_capacity > 0 :
					print("hascap: " + str(res_capacity))
					self.screen.get_screen("CreateReservationScreen").ids.reserve_card.add_widget(		
						MDChip( 
							text= str_counter,icon='clock', selected_chip_color=get_color_from_hex("#1C9554"),
							check=True,color=(112/255.0,211/255.0,47/255.0,1),on_release=self.fetch)
						)

				if res_capacity <= 0 :
					print("nocap: " + str(res_capacity))
					str_counter = str(counter) + " AM"
					self.screen.get_screen("CreateReservationScreen").ids.reserve_card.add_widget(		
						MDChip(
							text= str_counter,icon='clock', selected_chip_color=get_color_from_hex("#1C9554"),
							check=True,color=(253/255.0,78/255.0,72/255.0,1), on_release=self.unavailable)
						)

	def fetch(self,chip_instance):
		#get the value/time in the selected chip(time slot)
		if chip_instance.check == True:
			print (chip_instance.text)
			time = chip_instance.text
			CreateReservationScreen2.get_time(self,chip_instance.text)
		else:
			print('not')

	def unavailable(self,chip_instance):
		#called when time slot is unavailable
		if chip_instance.check == True:
			if not self.dialog:
				self.dialog = MDDialog(
					text="Sorry this slot is unavailable.",
					buttons=[
						MDFlatButton(
							text="Back", text_color=self.theme_cls.primary_color,  on_press=lambda new_screen: self.change_screen2(new_screen="CreateReservationScreen")
						),
					],
			)
			self.dialog.open()

	def create_reservation(self):
		CreateReservationScreen2.reserve(self)



	# ------------------------- #
	# MANAGER SCREEN FUNCTIONS  #
	# ------------------------- #


	def show_schedule_date_picker(self):
		min_date = datetime.strptime("2021:06:30",'%Y:%m:%d').date()
		max_date = datetime.strptime("2060:06:30",'%Y:%m:%d').date()
		date_dialog = MDDatePicker(min_date=min_date,max_date=max_date,primary_color=get_color_from_hex("#DAA520"),
			selector_color=get_color_from_hex("#DAA520"))
		date_dialog.bind(on_save=self.on_save_schedule)
		date_dialog.open()

	def on_save_schedule(self, instance, value, date_range):
		ManagerScheduleScreen.set_schedule(self, value)

	def reload_schedule(self, selected_date):
		date = datetime.strptime(selected_date, '%Y-%m-%d')
		ManagerScheduleScreen.set_schedule(self, date)

	def set_hr_availability(self, selected_date, selected_time):
		ManagerScheduleScreen.set_availability(self, selected_date, selected_time)

	def get_reservation_id(self, item_id):
		'''
			Getting the reservation item id fo be passed when 
			checking a reservation's details.
		'''
		UserItemScreen.set_manager_reservation_item(self, item_id)

	def showDeleteDialog(self):
		UserItemScreen.openDeleteDialog(self)

	def showEditDialog(self):
		UserItemScreen.openEditDialog(self)
		

	def deleteReservation(self, item_id):
		customer = self.screen.get_screen("UserItemScreen").ids.customer_name.text
		date = self.screen.get_screen("UserItemScreen").ids.date.text
		seats = self.screen.get_screen("UserItemScreen").ids.seats.text
		notes = self.screen.get_screen("UserItemScreen").ids.notes.text

		reservation = reservations.find_one({"customer_user": customer, "reservation_note":notes})
		print(reservation)

		res_id = reservation["_id"]
		reservations.delete_one({"_id": res_id})

		self.dialog_close(item_id)
		self.change_screen("ManagerReservationScreen","right")


MainApp().run()