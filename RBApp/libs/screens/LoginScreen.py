# Kivy imports
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

#Setting Window size
Window.size = (288, 565)

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations


class LoginScreen(Screen):
	
	def dialog_close(self, obj):
		self.dialog.dismiss()


	def checkUser(self):
		
		self.emailInput =self.screen.get_screen('LoginScreen').ids.email.text
		self.passInput =self.screen.get_screen('LoginScreen').ids.password.text

		res = customers.find_one({"email": self.emailInput})
		resManager = managers.find_one({"email": self.emailInput})

		if  ((res is not None) and (res.get('password')==self.passInput)) or ((resManager is not None) and (resManager.get('password')==self.passInput)):
			return True
					
		else:
			print("not present")
			if not self.dialog:
				self.dialog = MDDialog(
					text="Login Failed",
					buttons=[
						MDFlatButton(
							text="Try again", text_color=self.theme_cls.primary_color, on_release=self.dialog_close
						), 
					],				

			)
			self.screen.get_screen('LoginScreen').ids.email.text = ""
			self.screen.get_screen('LoginScreen').ids.password.text = ""
								
			self.dialog.open()




	



	
		
		