# Kivy imports
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore


# KivyMD imports
from kivymd.uix.list import TwoLineListItem

#Setting Window size
Window.size = (288, 565)

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations


store = JsonStore('temp.json')

class RestaurantsListItem(TwoLineListItem):
	'''
		Custom list item from the TwoLineListItem.
		Added attributes:
			item_id: Stores the restaurant id.
	'''
	item_id = ObjectProperty()
	
class RestaurantsScreen(Screen):
	def on_pre_enter(self):
		'''
			Loads the existing restaurants in the database before
			entering the screen.
		'''
		restaurants = managers.find().sort("restaurant_name", 1)
		self.ids.restaurants_list.clear_widgets()
		
		for r in restaurants:
			print(r)
			self.ids.restaurants_list.add_widget(
				RestaurantsListItem(
					item_id=r["_id"],
					text=r["restaurant_name"],
					secondary_text=r["restaurant_location"]
				)
			)