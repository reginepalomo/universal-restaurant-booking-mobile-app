# Kivy imports
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore


# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton

#Setting Window size
Window.size = (288, 565)


#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Manager
reservations = db.Reservations

store = JsonStore('temp.json')

class CustomerProfileScreen(Screen):
	def on_pre_enter(self):
		username = store["user"]["username"]
		customer = customers.find_one({"username": username})

		self.ids.username.text = customer['username']
		self.ids.email.text = customer['email']