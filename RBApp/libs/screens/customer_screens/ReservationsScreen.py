# Kivy imports
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore


# KivyMD imports
from kivymd.uix.list import ThreeLineListItem

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#Setting Window size
Window.size = (288, 565)

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

store = JsonStore('temp.json')

class ReservationsListItem(ThreeLineListItem):
	'''
		Custom list item from the ThreeLineListItem.
		Added attributes:
			item_id: Stores the reservation id.
	'''
	item_id = ObjectProperty()
	
class ReservationsScreen(Screen):
	def on_pre_enter(self):
		'''
			Loads the user's existing reservations before
			entering the screen.
		'''
		user_reservations = reservations.find({"customer_user": store["user"]["username"]}).sort([("reservation_date", pymongo.ASCENDING), ("reservation_hours", pymongo.ASCENDING)])

		self.ids.reservations_list.clear_widgets()
		
		for r in user_reservations:

			restaurant_loc = managers.find_one({"username":r["restaurant_user"]})["restaurant_location"]
			self.ids.reservations_list.add_widget(
				ReservationsListItem(
					item_id=r["_id"],
					text=r["restaurant_name"],
					secondary_text=restaurant_loc,
					tertiary_text=r["reservation_hours"] + ", " + str(r["reservation_date"])
				)
			)
		
