# Kivy imports
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton

#Setting Window size
Window.size = (288, 565)

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db connect
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

class RestaurantSearchItemScreen(Screen):
	def set_item(self, item_id):
		'''
			Sets which reservation item is being viewed.
		'''
		
		
		#Getting restaurant details from database from id passed from RestaurantSearchScreen
		restaurant = managers.find_one({"_id": item_id})

		self.screen.get_screen("RestaurantSearchItemScreen").ids.restaurant_name.text = restaurant['restaurant_name']
		self.screen.get_screen("RestaurantSearchItemScreen").ids.location.text = restaurant['restaurant_location']
		self.screen.get_screen("RestaurantSearchItemScreen").ids.time.text = restaurant['restaurant_hours']
		self.screen.get_screen("RestaurantSearchItemScreen").ids.cuisine.text = restaurant['restaurant_cuisine']
		self.screen.get_screen("RestaurantSearchItemScreen").ids.contact.text = restaurant['restaurant_contactnum']
