# Kivy imports
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton, MDFillRoundFlatButton
from kivymd.uix.picker import MDDatePicker
from kivymd.uix.chip import MDChip
from kivymd.uix.chip import MDChooseChip

#python import
import re

#Setting Window size
Window.size = (288, 565)


#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

from libs.screens.customer_screens.CreateReservationScreen2 import CreateReservationScreen2

class CreateReservationScreen(Screen):


	restoname = ""
	date = ""

	def get_resto_name(self,resto):
		global restoname 
		restoname = resto

	def get_date(self,selected_date):

		global date
		date = selected_date
		print(date)


	def reserve(self):
		pass

	

