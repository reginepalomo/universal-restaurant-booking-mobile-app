# Kivy imports
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore

# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#Setting Window size
Window.size = (288, 565)

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

store = JsonStore('temp.json')

from datetime import datetime

class ReservationItemScreen(Screen):

	def cancel_reservation(self):
		curr_user = store["user"]["username"]
		restaurant_name = self.screen.get_screen("ReservationItemScreen").ids.restaurant_name.text
		reservation_date = self.screen.get_screen("ReservationItemScreen").ids.reservation_date.text
		reservation_hours = self.screen.get_screen("ReservationItemScreen").ids.reservation_hours.text

		res_filter = {"$and" : [
			{"customer_user" : curr_user},
			{"restaurant_name" : restaurant_name},
			{"reservation_date" : reservation_date},
			{"reservation_hours" : reservation_hours}
			]}
		print(reservations.find_one(res_filter))
		reservations.delete_one(res_filter)

	def set_item(self, item_id):
		'''
			Sets which reservation item is being viewed.
		'''
		#Getting restaurant details from database from id passed from RestaurantsScreen
		reservation = reservations.find_one({"_id": item_id})
		
		self.screen.get_screen("ReservationItemScreen").ids.restaurant_name.text = reservation["restaurant_name"]
		self.screen.get_screen("ReservationItemScreen").ids.reservation_date.text = str(reservation["reservation_date"])
		self.screen.get_screen("ReservationItemScreen").ids.reservation_hours.text = reservation["reservation_hours"]
		self.screen.get_screen("ReservationItemScreen").ids.reservation_seats.text = str(reservation["reservation_seats"])
		self.screen.get_screen("ReservationItemScreen").ids.reservation_note.text = reservation["reservation_note"]

