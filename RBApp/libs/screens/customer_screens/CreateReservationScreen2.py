# Kivy imports
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore

# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton
from kivymd.uix.picker import MDDatePicker
from kivymd.uix.chip import MDChip
from kivymd.uix.chip import MDChooseChip
from kivymd.uix.dialog import MDDialog

#Setting Window size
Window.size = (288, 565)


#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

from libs.screens.customer_screens.RestaurantItemScreen import RestaurantItemScreen

store = JsonStore('temp.json')

class CreateReservationScreen2(Screen):

	time = "10AM"
	date = "2021-06-30"

	def get_date(self,selected_date):
		global date
		date = selected_date

	def get_time(self,selected_time):
		global time
		time = selected_time
		print(time)

	
	def on_pre_enter(self):	

		self.ids.date.text = date
		self.ids.time.text = time

	def reserve(self):

		resto_name = self.screen.get_screen('RestaurantItemScreen').ids.restaurant_name.text
		manager = managers.find_one({"restaurant_name": resto_name})


		self.customerInput = username = store["user"]["username"]
		self.restoInput = manager["username"]
		self.restoNameInput = self.screen.get_screen('RestaurantItemScreen').ids.restaurant_name.text
		self.hrsInput = time.replace(" ","")
		self.dateInput = str(date)
		self.seatsInput = int(self.screen.get_screen('CreateReservationScreen2').ids.seat.text)
		self.noteInput = self.screen.get_screen('CreateReservationScreen2').ids.note.text

		reservations.insert_one(
			{
			'restaurant_user': self.restoInput,
			'restaurant_name': self.restoNameInput,
			'customer_user': self.customerInput,
			'reservation_hours': self.hrsInput,
			'reservation_date': self.dateInput,
			'reservation_seats': self.seatsInput,
			'reservation_note': self.noteInput
			})

		if not self.dialog:
			self.dialog = MDDialog(
				text="Successfully reserved a slot!",
				buttons=[
					MDFlatButton(
						text="Done", text_color=self.theme_cls.primary_color,  on_press=lambda new_screen: self.change_screen2(new_screen="RestaurantItemScreen")
					),
				],
		)
		self.dialog.open()

		
			

