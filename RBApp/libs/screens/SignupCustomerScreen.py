# Kivy imports
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

#Setting Window size
Window.size = (288, 565)


class SignupCustomerScreen(Screen):
	def create_customer(self):
		self.usernameInput = self.screen.get_screen('SignupCustomerScreen').ids.username.text
		self.nameInput = self.screen.get_screen('SignupCustomerScreen').ids.fullname.text
		self.phoneInput = self.screen.get_screen('SignupCustomerScreen').ids.phonenumber.text
		self.emailInput =self.screen.get_screen('SignupCustomerScreen').ids.email.text
		self.passInput =self.screen.get_screen('SignupCustomerScreen').ids.password.text
		self.confirmInput =self.screen.get_screen('SignupCustomerScreen').ids.confirmpass.text

		res = customers.find_one({"username": self.usernameInput})
		res2 = customers.find_one({ "email": self.emailInput})
		if ((res is None) and (self.passInput == self.confirmInput) and (is_email(self.emailInput)) and (res2 is None) )   :
			customers.insert_one(
			{
			'username': self.usernameInput,
			'customer_name': self.nameInput,
			'customer_contactnum': self.phoneInput,
			'email': self.emailInput,
			'password': self.passInput
			})
			if not self.dialog:
				self.dialog = MDDialog(
					text="Account Created",
					buttons=[
						MDFlatButton(
							text="Continue", text_color=self.theme_cls.primary_color,  on_press=lambda new_screen: self.change_screen2(new_screen="LoginScreen")
						),
					],
			)
			self.dialog.open()
	
		else :
			if not self.dialog:
				self.dialog = MDDialog(
					text="Invalid input",
					buttons=[
						MDFlatButton(
							text="Try again", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
						),
					],
			)
			self.dialog.open()

