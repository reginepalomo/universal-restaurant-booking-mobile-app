# Kivy imports
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

#Setting Window size
Window.size = (288, 565)




class SignupManagerScreen2(Screen):
	def create_manager(self):
		self.userInput = self.screen.get_screen('SignupManagerScreen').ids.username.text
		self.passInput =self.screen.get_screen('SignupManagerScreen').ids.password.text
		self.emailInput =self.screen.get_screen('SignupManagerScreen').ids.email.text	
		self.confirmInput =self.screen.get_screen('SignupManagerScreen').ids.confirmpass.text
		self.contactInput = self.screen.get_screen('SignupManagerScreen').ids.contactnumber.text

		self.restoInput =self.screen.get_screen('SignupManagerScreen2').ids.restoname.text
		self.locationInput =self.screen.get_screen('SignupManagerScreen2').ids.location.text
		self.cuisineInput =self.screen.get_screen('SignupManagerScreen2').ids.cuisine.text
		self.timeInput =self.screen.get_screen('SignupManagerScreen2').ids.time.text
		self.capInput =self.screen.get_screen('SignupManagerScreen2').ids.capacity.text
		
		

		res = managers.find_one({"username": self.userInput})
		res2 = managers.find_one({ "email": self.emailInput})
		if ((res is None) and (self.passInput == self.confirmInput) and (is_email(self.emailInput)) and (res2 is None) )   :
			managers.insert_one(
			{
			'username': self.userInput,
			'password': self.passInput,
			'email': self.emailInput,
			'restaurant_contactnum': self.contactInput,
			'restaurant_name': self.restoInput,
			'restaurant_cap': self.capInput,
			'restaurant_location': self.locationInput,
			'restaurant_cuisine': self.cuisineInput,
			'restaurant_hours': self.timeInput
						
			})
			if not self.dialog:
				self.dialog = MDDialog(
					text="Account Created",
					buttons=[
						MDFlatButton(
							text="Continue", text_color=self.theme_cls.primary_color,on_press=lambda new_screen: self.change_screen2(new_screen="LoginScreen")
						),
					],
			)
			self.dialog.open()

		else :
			if not self.dialog:
				self.dialog = MDDialog(
					text="Invalid input",
					buttons=[
						MDFlatButton(
							text="Try again", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
						),
					],
			)
			self.dialog.open()