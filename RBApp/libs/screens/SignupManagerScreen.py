# Kivy imports
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen


# KivyMD imports
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

#Setting Window size
Window.size = (288, 565)



class SignupManagerScreen(Screen):
	pass
