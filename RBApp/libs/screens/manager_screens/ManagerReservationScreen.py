# Kivy imports
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore

# KivyMD imports
from kivymd.uix.list import TwoLineListItem

#Setting Window size
Window.size = (288, 565)

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db connect
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations


store = JsonStore('temp.json')

class UsersListItem(TwoLineListItem):
	'''
		Custom list item from the TwoLineListItem.
		Added attributes:
			item_id: Stores the reservation id.
	'''
	item_id = ObjectProperty()

	
class ManagerReservationScreen(Screen):
	def on_pre_enter(self):
		'''
			Loads the user's existing reservations before
			entering the screen.
		'''
		
		#query all reservation of resto from reservations table in db
		reservationsList = reservations.find({"restaurant_user": store["user"]["username"]}).sort([("reservation_date", pymongo.ASCENDING), ("reservation_hours", pymongo.ASCENDING)])

		self.ids.reservations_list.clear_widgets()
		for r in reservationsList:
			if r['customer_user'] != "":
				reservationDate = r["reservation_date"]

				self.ids.reservations_list.add_widget(
					UsersListItem(
						item_id=r["_id"],
						text=r["customer_user"],
						secondary_text= reservationDate
					)
				)
