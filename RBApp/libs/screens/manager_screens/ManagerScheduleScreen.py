# Kivy imports
from kivy.core.window import Window
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty, StringProperty
from kivy.storage.jsonstore import JsonStore


# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton
from kivymd.uix.list import ThreeLineAvatarListItem, ImageLeftWidget

#Setting Window size
Window.size = (288, 565)


#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

from datetime import datetime

#db connect
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

store = JsonStore('temp.json')

class ManagerScheduleItem(BoxLayout):
	time = StringProperty()
	reservation_count = StringProperty()
	status = StringProperty()
	color = ObjectProperty()
	date = StringProperty()

class ManagerScheduleScreen(Screen):
	def on_pre_enter(self):

		# Getting the reservations of the current user on current date
		curr_user = store["user"]["username"]
		curr_date = datetime.today()
		res_filter = {"$and" : [
			{"restaurant_user" : curr_user},
			{"reservation_date" : curr_date.strftime("%Y-%m-%d")}
			]}
		curr_reservations = reservations.find(res_filter)

		aaa = reservations.find({"restaurant_user": curr_user})
		for a in aaa:
			print(a)

		# Setting current date label
		self.ids.date.text = str(curr_date.strftime("%A, %B %d"))

		# Getting the restaurant's open hours for schedule
		manager_acc = managers.find_one({"username" : curr_user})
		open_hours = manager_acc["restaurant_hours"]
		print(open_hours)

		open_time = open_hours.split("-")[0]
		open_period = "AM"
		if open_time.count("PM") is not 0:
			open_period = "PM"
		open_hour = open_time.replace(open_period, "")

		close_time = open_hours.split("-")[1]

		# Checking if  the restaurant is still open for reservations
		res_open = True #still open for reservations (capacity not full)
		res_capacity = int(manager_acc["restaurant_cap"])

		for r in curr_reservations:
			res_capacity = res_capacity - r["reservation_seats"]

		if res_capacity <= 0:
			res_open = False #full capacity

		#Loop initial values
		loop = True 
		curr_time = open_hour + open_period
		curr_hour = int(open_hour)
		curr_period = open_period

		while(loop):
			#from db values
			res_hr_filter = {"$and" : [
				{"restaurant_user" : curr_user},
				{"reservation_date" : curr_date.strftime("%Y-%m-%d")}
			]}
			#print(curr_time)

			curr_res_count = 0 
			curr_status = True
			curr_hr_reservations = reservations.find(res_hr_filter)
			for r in curr_hr_reservations:
				if r["reservation_hours"] == curr_time:
					print(r["reservation_hours"])
					if len(r["customer_user"]) > 0:
						curr_res_count = curr_res_count + 1
					else:
						curr_status = False

			# Default colors / button label
			curr_color = (112/255.0,211/255.0,47/255.0,1)
			status_text = "Available"
			if res_open:
				if curr_res_count > 0:
					curr_color = (255/255.0,164/255.0,55/255.0,1)

				if not curr_status:
					curr_color = (253/255.0,78/255.0,72/255.0,1)
					status_text = "Unavailable"
			else:
				curr_color = (253/255.0,78/255.0,72/255.0,1)
				status_text = "Unavailable"


			self.ids.schedule_list.add_widget(
				ManagerScheduleItem(
					time = curr_time,
					reservation_count = str(curr_res_count),
					status = status_text,
					color = curr_color,
					date = curr_date.strftime("%Y-%m-%d"))
			)
			prev_hour = curr_hour
			curr_hour = curr_hour + 1 
			if prev_hour is 12:
				curr_hour = 1
				if curr_period == "AM":
					curr_period = "PM"

			curr_time = str(curr_hour) + curr_period

			if curr_time == close_time:
				loop = False

		self.ids.schedule_list.add_widget(BoxLayout())

	def set_schedule(self, selected_date):
		self.screen.get_screen('ManagerScheduleScreen').ids.schedule_list.clear_widgets()

		print(selected_date)
		# Getting the reservations of the current user on current date
		curr_user = store["user"]["username"]
		res_filter = {"$and" : [
			{"restaurant_user" : curr_user},
			{"reservation_date" : selected_date.strftime('%Y-%m-%d')}
		]}
		curr_reservations = reservations.find(res_filter)
		#print(curr_reservations)

		# Setting current date label
		self.screen.get_screen('ManagerScheduleScreen').ids.date.text = str(selected_date.strftime("%A, %B %d"))

		# Getting the restaurant's open hours for schedule
		manager_acc = managers.find_one({"username" : curr_user})
		open_hours = manager_acc["restaurant_hours"]
		#print(open_hours)

		open_time = open_hours.split("-")[0]
		open_period = "AM"
		if open_time.count("PM") is not 0:
			open_period = "PM"
		open_hour = open_time.replace(open_period, "")

		close_time = open_hours.split("-")[1]

		# Checking if  the restaurant is still open for reservations
		res_open = True #still open for reservations (capacity not full)
		res_capacity = int(manager_acc["restaurant_cap"])
		
		for r in curr_reservations:
			res_capacity = res_capacity - r["reservation_seats"]

		if res_capacity <= 0:
			res_open = False #full capacity

		#Loop initial values
		loop = True 
		curr_time = open_hour + open_period
		curr_hour = int(open_hour)
		curr_period = open_period

		while(loop):
			#from db values
			res_hr_filter = {"$and" : [
				{"restaurant_user" : curr_user},
				{"reservation_date" : selected_date.strftime("%Y-%m-%d")}
			]}
			#print(curr_time)

			curr_res_count = 0 
			curr_status = True
			curr_hr_reservations = reservations.find(res_hr_filter)
			for r in curr_hr_reservations:
				if r["reservation_hours"] == curr_time:
					if len(r["customer_user"]) > 0:
						curr_res_count = curr_res_count + 1
					else:
						curr_status = False

			# Default colors / button label
			curr_color = (112/255.0,211/255.0,47/255.0,1)
			status_text = "Available"
			if res_open:
				if curr_res_count > 0:
					curr_color = (255/255.0,164/255.0,55/255.0,1)

				if not curr_status:
					curr_color = (253/255.0,78/255.0,72/255.0,1)
					status_text = "Unavailable"
			else:
				curr_color = (253/255.0,78/255.0,72/255.0,1)
				status_text = "Unavailable"


			self.screen.get_screen('ManagerScheduleScreen').ids.schedule_list.add_widget(
				ManagerScheduleItem(
					time = curr_time,
					reservation_count = str(curr_res_count),
					status = status_text,
					color = curr_color,
					date = selected_date.strftime("%Y-%m-%d"))
			)
			prev_hour = curr_hour
			curr_hour = curr_hour + 1 
			if prev_hour is 12:
				curr_hour = 1
				if curr_period == "AM":
					curr_period = "PM"

			curr_time = str(curr_hour) + curr_period

			if curr_time == close_time:
				loop = False

		self.screen.get_screen('ManagerScheduleScreen').ids.schedule_list.add_widget(BoxLayout())

	def set_availability(self, selected_date, selected_time):
		#Create entry
		restaurant_info = managers.find_one({"username" : store["user"]["username"]})

		#Check if it is already unavailable
		res_hr_filter = {"$and" : [
				{"restaurant_user" : store["user"]["username"]},
				{"reservation_date" : selected_date},
				{"reservation_hours" : selected_time},
				{"customer_user" : ""}
			]}
		curr_hr_reservation = reservations.find_one(res_hr_filter)

		if curr_hr_reservation:
			reservations.delete_one({"_id":curr_hr_reservation["_id"]})
		else:
			unavailable_entry = {
				"restaurant_user" : store["user"]["username"],
				"restaurant_name" : restaurant_info["restaurant_name"],
				"customer_user" : "",
				"reservation_hours" : selected_time,
				"reservation_date" : selected_date,
				"reservation_seats" : 0,
				"reservation_note" : "unavailable"
			}
			reservations.insert_one(unavailable_entry)




