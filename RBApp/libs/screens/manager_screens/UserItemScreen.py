# Kivy imports
from kivy.core.window import Window
from kivy.properties import StringProperty
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.dialog import MDDialog
from kivymd.uix.button import MDFlatButton

#Setting Window size
Window.size = (288, 565)

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db connect
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations


class UserItemScreen(Screen):
	def set_manager_reservation_item(self, item_id):
		'''
			Sets which reservation item is being viewed.
		'''
		self.screen.get_screen("UserItemScreen").ids.reservation_item.clear_widgets()

		#get reservation details
		reservation = reservations.find_one({"_id": item_id})
		customer = customers.find_one({"username": reservation['customer_user']})

		self.screen.get_screen("UserItemScreen").ids.customer_name.text = reservation['customer_user']
		self.screen.get_screen("UserItemScreen").ids.customer_email.text = customer['email']
		self.screen.get_screen("UserItemScreen").ids.date.text = reservation['reservation_date']
		self.screen.get_screen("UserItemScreen").ids.seats.text = str(reservation['reservation_seats'])
		self.screen.get_screen("UserItemScreen").ids.notes.text = reservation['reservation_note']

	def openDeleteDialog(self):

		if not self.dialog:
			self.dialog = MDDialog(
				text="Are you sure you want to delete this resevation?",
				buttons=[
					MDFlatButton(
						text="cancel", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
					),
					MDFlatButton(
						text="delete", text_color= self.theme_cls.primary_color,on_press=self.deleteReservation
					),
				],
			)
		self.dialog.open()

	def openEditDialog(self):
		if not self.dialog:
			self.dialog = MDDialog(
				text="Are you sure you want to save the changes?",
				buttons=[
					MDFlatButton(
						text="cancel", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
					),
					MDFlatButton(
						text="confirm", text_color= self.theme_cls.primary_color
					),
				],
			)
		self.dialog.open()


	



			

		