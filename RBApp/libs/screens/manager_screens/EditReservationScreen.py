# Kivy imports
from kivy.core.window import Window
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.screenmanager import Screen
from kivy.storage.jsonstore import JsonStore

# KivyMD imports
from kivymd.uix.list import ThreeLineAvatarListItem, ImageLeftWidget

#Setting Window size
Window.size = (288, 565)

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db connect
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations


store = JsonStore('temp.json')

class EditReservationScreen(Screen):

	def editReservation(self):


		customer = self.screen.get_screen("UserItemScreen").ids.customer_name.text
		date = self.screen.get_screen("UserItemScreen").ids.date.text
		seats = self.screen.get_screen("UserItemScreen").ids.seats.text
		notes = self.screen.get_screen("UserItemScreen").ids.notes.text

		reservation = reservations.find_one({"customer_user": customer,"reservation_note":notes})
		print(reservation)

		self.screen.get_screen("EditReservationScreen").ids.time.text = reservation['reservation_hours']
		self.screen.get_screen("EditReservationScreen").ids.days.text = reservation['reservation_date'].strftime('%c')
		self.screen.get_screen("EditReservationScreen").ids.seats.text = str(reservation['reservation_seats'])
		self.screen.get_screen("EditReservationScreen").ids.notes.text = reservation['reservation_note']
	

