# Kivy imports
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen
from kivy.properties import ObjectProperty, StringProperty
from kivy.storage.jsonstore import JsonStore

# KivyMD imports
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton
from kivymd.uix.list import ThreeLineAvatarListItem, ImageLeftWidget

#Setting Window size
Window.size = (288, 565)


#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email


#db connect
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
customers = db.Customer
managers = db.Restaurant
reservations = db.Reservations

store = JsonStore('temp.json')

class ManagerProfileScreen(Screen):
	def on_pre_enter(self):
		'''
			Sets which reservation item is being viewed.
		'''
		
		#get resto details
		username = store["user"]["username"]
		restaurant = managers.find_one({"username": username})

		self.ids.restaurant_name.text = restaurant['restaurant_name']
		self.ids.location.text = restaurant['restaurant_location']
		self.ids.time.text = restaurant['restaurant_hours']
		self.ids.cuisine.text = restaurant['restaurant_cuisine']
		self.ids.contact.text = restaurant['restaurant_contactnum']
		self.ids.user.text = restaurant['username']
		self.ids.user_email.text = restaurant['email']

		
		

		



