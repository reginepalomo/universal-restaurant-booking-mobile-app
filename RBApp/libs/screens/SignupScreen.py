# Kivy imports
from kivy.core.window import Window
from kivy.uix.screenmanager import Screen

# KivyMD imports
from kivymd.uix.button import MDFlatButton

#db imports
import pymongo
from pymongo import MongoClient
from pyisemail import is_email

#db creds
MONGO_URL = "mongodb+srv://reg:pass@cluster0.sskbf.mongodb.net/project?retryWrites=true&w=majority"
cluster = MongoClient(MONGO_URL)
db = cluster['project']
user = db.users

#Setting Window size
Window.size = (288, 565)


class SignupScreen(Screen):
	pass

class SignupCustomerScreen(Screen):
	def create_customer(self):
		self.userInput = self.screen.get_screen('SignupCustomerScreen').ids.fullname.text
		self.phoneInput = self.screen.get_screen('SignupCustomerScreen').ids.phonenumber.text
		self.emailInput =self.screen.get_screen('SignupCustomerScreen').ids.email.text
		self.passInput =self.screen.get_screen('SignupCustomerScreen').ids.password.text
		self.confirmInput =self.screen.get_screen('SignupCustomerScreen').ids.confirmpass.text

		res = user.find_one({"fullname": self.userInput})
		res2 = user.find_one({ "email": self.emailInput})
		if ((res is None) and (self.passInput == self.confirmInput) and (is_email(self.emailInput)) and (res2 is None) )   :
			user.insert_one(
			{
			'fullname': self.userInput,
			'phone_number': self.phoneInput,
			'email': self.emailInput,
			'password': self.passInput
			})
			if not self.dialog:
				self.dialog = MDDialog(
					text="Account Created",
					buttons=[
						MDFlatButton(
							text="Continue", text_color=self.theme_cls.primary_color
						),
					],
			)
			self.dialog.open()
	
		else :
			if not self.dialog:
				self.dialog = MDDialog(
					text="Invalid input",
					buttons=[
						MDFlatButton(
							text="Try again", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
						),
					],
			)
			self.dialog.open()

class SignupManagerScreen(Screen):
	pass

class SignupManagerScreen2(Screen):
	def create_manager(self):
		self.userInput = self.screen.get_screen('SignupManagerScreen').ids.username.text
		self.contactInput = self.screen.get_screen('SignupManagerScreen').ids.contactnumber.text
		self.emailInput =self.screen.get_screen('SignupManagerScreen').ids.email.text
		self.passInput =self.screen.get_screen('SignupManagerScreen').ids.password.text
		self.confirmInput =self.screen.get_screen('SignupManagerScreen').ids.confirmpass.text

		self.restoInput =self.screen.get_screen('SignupManagerScreen2').ids.restoname.text
		self.locationInput =self.screen.get_screen('SignupManagerScreen2').ids.location.text
		self.capacityInput =self.screen.get_screen('SignupManagerScreen2').ids.capacity.text
		self.cuisineInput =self.screen.get_screen('SignupManagerScreen2').ids.cuisine.text

		res = user.find_one({"username": self.userInput})
		res2 = user.find_one({ "email": self.emailInput})
		if ((res is None) and (self.passInput == self.confirmInput) and (is_email(self.emailInput)) and (res2 is None) )   :
			user.insert_one(
			{
			'username': self.userInput,
			'contact_number': self.contactInput,
			'email': self.emailInput,
			'password': self.passInput,
			'restaurant_name': self.restoInput,
			'location': self.locationInput,
			'capacity': self.capacityInput,
			'cuisine': self.cuisineInput
			})
			if not self.dialog:
				self.dialog = MDDialog(
					text="Account Created",
					buttons=[
						MDFlatButton(
							text="Continue", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
						),
					],
			)
			self.dialog.open()

		else :
			if not self.dialog:
				self.dialog = MDDialog(
					text="Invalid input",
					buttons=[
						MDFlatButton(
							text="Try again", text_color=self.theme_cls.primary_color,on_release=self.dialog_close
						),
					],
			)
			self.dialog.open()